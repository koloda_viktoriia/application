package com.bsa.application.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bsa.application.R
import com.bsa.application.data.model.Post
import com.bsa.application.databinding.PostItemBinding
import com.bsa.application.viewmodel.PostItemClickListener

class PostsAdapter(
    private val posts: List<Post>,
    private val listener: PostItemClickListener
) : RecyclerView.Adapter<PostsAdapter.PostsViewHolder>() {
    override fun getItemCount() = posts.size
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = PostsViewHolder(
        DataBindingUtil.inflate<PostItemBinding>(
            LayoutInflater.from(parent.context),
            R.layout.post_item,
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: PostsViewHolder, position: Int) {
        holder.recyclerviewPostItemBinding.post = posts[position]
        holder.recyclerviewPostItemBinding.root.setOnClickListener {
            listener.onPostItemClick(holder.recyclerviewPostItemBinding.root, posts[position])
        }

    }

    inner class PostsViewHolder(
        val recyclerviewPostItemBinding: PostItemBinding
    ) : RecyclerView.ViewHolder(recyclerviewPostItemBinding.root)
}