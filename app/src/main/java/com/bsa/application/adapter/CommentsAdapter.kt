package com.bsa.application.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bsa.application.R
import com.bsa.application.data.model.Comment
import com.bsa.application.databinding.CommentItemBinding

class CommentsAdapter(
    private val comments: List<Comment>
) : RecyclerView.Adapter<CommentsAdapter.PostsViewHolder>() {
    override fun getItemCount() = comments.size
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = PostsViewHolder(
        DataBindingUtil.inflate<CommentItemBinding>(
            LayoutInflater.from(parent.context),
            R.layout.comment_item,
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: PostsViewHolder, position: Int) {
        holder.recyclerviewCommentItemBinding.comment = comments[position]
    }

    inner class PostsViewHolder(
        val recyclerviewCommentItemBinding: CommentItemBinding
    ) : RecyclerView.ViewHolder(recyclerviewCommentItemBinding.root)
}