package com.bsa.application.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bsa.application.data.model.Post
import com.bsa.application.data.repository.Repository
import com.bsa.application.util.Coroutines
import kotlinx.coroutines.Job

class PostsListViewModel(
    private val repository: Repository,
    private val position: Int
) : ViewModel() {
    private lateinit var job: Job

    private val _posts = MutableLiveData<List<Post>>()
    val posts: LiveData<List<Post>>
        get() = _posts


    fun getPosts() {
        if (position == 0) {
            job = Coroutines.ioTheMain(
                { repository.getPosts() },
                { _posts.value = it }
            )
        } else {
            job = Coroutines.ioTheMain(
                { repository.getPostsFromDB() },
                { _posts.value = it }
            )

        }
    }

    override fun onCleared() {
        super.onCleared()
        if (::job.isInitialized) job.cancel()
    }
}