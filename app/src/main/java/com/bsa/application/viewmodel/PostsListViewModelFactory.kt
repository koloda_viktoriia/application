package com.bsa.application.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.bsa.application.data.repository.Repository

@Suppress("UNCHECKED_CAST")
class PostsListViewModelFactory(
    private val repository: Repository,
    private val position: Int
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return PostsListViewModel(repository, position) as T
    }
}