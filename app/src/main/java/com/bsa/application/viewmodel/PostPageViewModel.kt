package com.bsa.application.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bsa.application.data.model.Comment
import com.bsa.application.data.model.Post
import com.bsa.application.data.model.User
import com.bsa.application.data.repository.Repository
import com.bsa.application.util.Coroutines
import kotlinx.coroutines.Job

class PostPageViewModel(
    private val repository: Repository,
    private val post: Post
) : ViewModel() {
    private lateinit var job: Job

    private val _comments = MutableLiveData<List<Comment>>()
    val comments: LiveData<List<Comment>>
        get() = _comments

    private val _users = MutableLiveData<List<User>>()
    val users: LiveData<List<User>>
        get() = _users

    fun savePost() {
        job = Coroutines.ToDB { repository.savePost(post) }

    }

    fun getUsers() {
        job = Coroutines.ioTheMain(
            { repository.getUsers(post.userId) },
            { _users.value = it }
        )
    }


    fun getComments() {
        job = Coroutines.ioTheMain(
            { repository.getComments(post.id) },
            { _comments.value = it }
        )
    }


    override fun onCleared() {
        super.onCleared()
        if (::job.isInitialized) job.cancel()
    }
}