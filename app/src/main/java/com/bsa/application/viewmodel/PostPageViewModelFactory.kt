package com.bsa.application.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.bsa.application.data.model.Post
import com.bsa.application.data.repository.Repository

@Suppress("UNCHECKED_CAST")
class PostPageViewModelFactory(
    private val repository: Repository,
    private val post: Post
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return PostPageViewModel(
            repository,
            post
        ) as T
    }
}