package com.bsa.application.viewmodel

import android.view.View
import com.bsa.application.data.model.Post

interface PostItemClickListener {
    fun onPostItemClick(view: View, post: Post)
}