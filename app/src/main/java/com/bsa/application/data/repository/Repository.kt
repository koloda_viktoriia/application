package com.bsa.application.data.repository

import com.bsa.application.data.database.PostEntity
import com.bsa.application.data.database.RoomlApplication
import com.bsa.application.data.model.Post
import com.bsa.application.data.network.API

class Repository(
    private val api: API
) : SaveApiRequest() {
    suspend fun getPosts() = apiRequest { api.getPosts() }
    suspend fun getComments(postId: Int) = apiRequest { api.getComments(postId) }
    suspend fun getUsers(userId: Int) = apiRequest { api.getUsers(userId) }
    fun savePost(post: Post) {
        var savedPost: PostEntity = PostEntity(post.body, post.id, post.title, post.userId)
       RoomlApplication.database!!.postDao().addPost(savedPost)
    }

    fun getPostsFromDB(): List<Post> {
        var data = RoomlApplication.database!!.postDao().getAllPosts()
        return data
    }
}
