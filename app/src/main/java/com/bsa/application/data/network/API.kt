package com.bsa.application.data.network

import com.bsa.application.data.model.Comment
import com.bsa.application.data.model.Post
import com.bsa.application.data.model.User
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface API {
    @GET("posts")
    suspend fun getPosts(): Response<List<Post>>

    @GET("comments")
    suspend fun getComments(@Query("postId") postId: Int?): Response<List<Comment>>

    @GET("users")
    suspend fun getUsers(@Query("id") userId: Int?): Response<List<User>>

    companion object {
        operator fun invoke(): API {
            return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .build()
                .create(API::class.java)
        }
    }
}