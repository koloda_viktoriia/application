package com.bsa.application.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.bsa.application.data.model.Post

@Dao
interface PostDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addPost(post: PostEntity)

    @Query("SELECT * FROM Post")
    fun getAllPosts(): List<Post>


}