package com.bsa.application.data.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Post")
data class PostEntity(
    val body: String,
    @field:PrimaryKey
    val id: Int,
    val title: String,
    val userId: Int
)