package com.bsa.application.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bsa.application.R
import com.bsa.application.data.model.Post
import com.bsa.application.ui.fragment.PostPage

class PostActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.post_page_activity)
        val intent: Intent = getIntent()
        val post: Post? = intent.getParcelableExtra<Post>("post")
        val mFragment = PostPage()
        val mArgs = Bundle()
        mArgs.putParcelable("post", post)
        mFragment.setArguments(mArgs)


    }
}