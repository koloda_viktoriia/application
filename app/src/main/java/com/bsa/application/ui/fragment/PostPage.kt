package com.bsa.application.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.bsa.application.R
import com.bsa.application.adapter.CommentsAdapter
import com.bsa.application.data.model.Post
import com.bsa.application.data.network.API
import com.bsa.application.data.repository.Repository
import com.bsa.application.viewmodel.PostPageViewModel
import com.bsa.application.viewmodel.PostPageViewModelFactory
import kotlinx.android.synthetic.main.post_page_fragment.*

class PostPage() : Fragment() {

    private lateinit var viewModel: PostPageViewModel
    private lateinit var factory: PostPageViewModelFactory
    lateinit var post: Post


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val bundle = activity!!.intent.extras
        post = bundle!!.getParcelable<Post>("post")!!
        return inflater.inflate(R.layout.post_page_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val api = API()
        val repository = Repository(api)
        factory =
            PostPageViewModelFactory(
                repository,
                post
            )
        viewModel = ViewModelProviders.of(this, factory).get(PostPageViewModel::class.java)
        PostBody.text = post.body
        PostTitle.text = post.title
        viewModel.savePost()
        viewModel.getUsers()
        viewModel.users.observe(viewLifecycleOwner, Observer { user ->
            UserName.text = user.get(0).username
            UserEmail.text = user.get(0).email
            UserPhone.text = user.get(0).phone
            UserWebsite.text = user.get(0).website
        })
        viewModel.getComments()
        viewModel.comments.observe(viewLifecycleOwner, Observer { comments ->
            recycler_view_comments.also {
                it.layoutManager = LinearLayoutManager(requireContext())
                it.setHasFixedSize(true)
                it.adapter =
                    CommentsAdapter(comments)
            }

        })

    }

}