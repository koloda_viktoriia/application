package com.bsa.application.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bsa.application.R
import com.bsa.application.adapter.PagerAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val fragmentAdapter = PagerAdapter(supportFragmentManager)
        viewpager.adapter = fragmentAdapter
        tabBar.setupWithViewPager(viewpager)

    }
}