package com.bsa.application.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.bsa.application.R
import com.bsa.application.adapter.PostsAdapter
import com.bsa.application.data.model.Post
import com.bsa.application.data.network.API
import com.bsa.application.data.repository.Repository
import com.bsa.application.ui.PostActivity
import com.bsa.application.viewmodel.PostItemClickListener
import com.bsa.application.viewmodel.PostsListViewModel
import com.bsa.application.viewmodel.PostsListViewModelFactory
import kotlinx.android.synthetic.main.posts_list_fragment.*

class PostsList(var position: Int) : Fragment(),
    PostItemClickListener {

    private lateinit var factory: PostsListViewModelFactory
    private lateinit var viewModel: PostsListViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.posts_list_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val api = API()
        val repository = Repository(api)
        factory =
            PostsListViewModelFactory(repository, position)
        viewModel = ViewModelProviders.of(this, factory).get(PostsListViewModel::class.java)
        viewModel.getPosts()
        viewModel.posts.observe(viewLifecycleOwner, Observer { posts ->
            recycler_view_posts.also {
                it.layoutManager = LinearLayoutManager(requireContext())
                it.setHasFixedSize(true)
                it.adapter =
                    PostsAdapter(posts, this)
            }

        })

    }

    override fun onPostItemClick(view: View, post: Post) {
        val intent = Intent(activity, PostActivity::class.java)
        intent.putExtra("post", post)
        activity?.startActivity(intent)
    }
}